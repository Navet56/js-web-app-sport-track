var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var session = require('express-session');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use('/', indexRouter);
app.use('/users', usersRouter);

app.use(session({  name: 'session',
                   secret: 'secret',
                   saveUninitialized: true,
                   resave: true,
                   cookie: {secure: false,
                             maxAge: 60000}
              }));

var create_account = require("./routes/create-account");
app.use("/create-account", create_account);

var connect_account = require("./routes/connect-account");
app.use("/connect-account", connect_account);

var valid_connect = require("./routes/valid-connect");
app.use("/valid-connect", valid_connect);

var valid_upload = require("./routes/valid_upload");
app.use("/valid_upload", valid_upload);

var add_account = require("./routes/add-account");
app.use("/add-account", add_account);

var activities = require("./routes/activities");
app.use("/activities", activities);

var upload = require("./routes/upload");
app.use("/upload", upload);

var modify = require("./routes/modify");
app.use("/modify", modify);

var disconnect = require("./routes/disconnect");
app.use("/disconnect", disconnect);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  console.log("err : " + err);
  // set locals, only providing error in development
  console.log(err.message);
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});


module.exports = app;
