const express = require("express");
const multer = require("multer");
const fs = require("fs");
const path = require("path");
const router = express.Router();
const act_dao = require('sport-track-db').activity_dao;
const act_entry_dao = require('sport-track-db').act_entry_dao;
const utils = require('sport-track-db').utils;

const app = express();

const upload = multer({
    dest: "routes/" // "uploads"
});


async function makeInserts(activityArray, fullDataArray){
    try {
        let act_id = await act_dao.insert(activityArray);
        insertIntoData(fullDataArray, act_id);
    } catch (error) {
        console.log(error);
    }
}

async function insertIntoData(fullDataArray, act_id){
    try{

        for (let dataDict of fullDataArray){
            let dataArray = [];
            for (var key in dataDict){
                dataArray.push(dataDict[key]);
            }
            dataArray.push(act_id)
            await act_entry_dao.insert(dataArray);
        }


    } catch (error) {
        console.log(error);
    }


}


router.post("/", upload.single("fileToUpload"), (req, res, next) => {
    if (req.session.email == undefined) {
       res.render('error', {});
     } else {
        const absolutePath = path.join(__dirname, req.file.filename);
        const jsonString = fs.readFileSync(absolutePath, "utf-8");
        fs.unlinkSync(absolutePath);
        const jsonObject = JSON.parse(jsonString);

        var activityArray = utils.makeActivityArray(jsonObject);

        let email = req.session.email;
        activityArray.push(email);

        let fullDataArray = utils.readData(jsonObject);

        makeInserts(activityArray, fullDataArray);

        res.render("valid-upload", {});
     }
});

module.exports = router;
