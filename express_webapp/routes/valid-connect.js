   var express = require('express');
   var router = express.Router();
   var user_dao = require('sport-track-db').user_dao;

   router.post("/", function(req, res, next){

      user_dao.findByKey(req.body.email, function(err, rows) {
         if(err != null){
           console.log("ERROR= " +err);
         }else {
           if(req.body.psw == rows['password']) {
              console.log('Connecting');
              req.session.email = rows['email'];
              req.session.save();
              res.render("valid-connect", {});
           } else {
              res.render("error"); //mot de passe incorrect
           }
         }
       });

   });
   module.exports = router;
