   var express = require('express');
   var router = express.Router();
   router.get("/", function(req, res, next){
     try {
       req.session.destroy();
       res.render("disconnect", {});
     } catch (e) {
        res.render("error", {});
     }
   });

   module.exports = router;
