var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {
  if (req.session.email == undefined) {
     res.render('error', {});
  } else
    res.render('modify', {});
});

module.exports = router;
