# SportTrack JS

Version JavaScript de l'application SportTrack faite en PHP.

Par Evan DIBERDER (B1) et Cedric SIMAR (B2)

## Installer les dépendances (node-modules) :

`./install_modules.sh`

## Lancer l'application SportTrack directement

`./run.sh` 
