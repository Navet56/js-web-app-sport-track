const act_dao = require("./activity_dao");
const dayjs = require("dayjs");

class util {
  calculDistance2PointsGPS(lat1, long1, lat2, long2){
    let R = 6378.137 * 1000;
    lat1 = this.deg2rad(lat1);
    lat2 = this.deg2rad(lat2);
    long1 = this.deg2rad(long1);
    long2 = this.deg2rad(long2);
    let distance = R*Math.acos(Math.sin(lat2)*Math.sin(lat1)+Math.cos(lat2)*Math.cos(lat1)*Math.cos(long2-long1));
    return distance;

  }

  deg2rad(d){
    return d * (Math.PI)/180;
  }


  calculDistanceTrajet(parcours){
      let distance = 0.0;
      for (let i=0; i < parcours.length - 1;  i++) {
        distance += this.calculDistance2PointsGPS(parcours[i]['latitude'], parcours[i]['longitude'], parcours[i+1]['latitude'],  parcours[i+1]['longitude'])
      }
      return distance;
  }

  makeActivityArray(json){
    let act_info = json.activity;

    let date = act_info['date'];
    let desc = act_info['description'];

    let dist = this.calculDistanceTrajet(json['data']);
    let heure_debut = json['data'][0]['time'];
    let heure_fin = json['data'][(json['data']).length -1]['time'];


    let dateDebut = dayjs(`${date} ${heure_debut}`);
    let dateFin = dayjs(`${date} ${heure_fin}`);

    let duree = dateFin.diff(dateDebut, "second");


    let cardio_array = [];
    for(let i = 0; i < json['data'].length; i++){
      cardio_array[i] = json['data'][i]['cardio_frequency'];
    }

    let cmin = Math.min.apply(Math, cardio_array);
    let cmax = Math.max.apply(Math, cardio_array);
    const reducer = (accumulator, currentValue) => accumulator + currentValue;
    let cavg = cardio_array.reduce(reducer)/cardio_array.length;


    let array = [date, desc, heure_debut, duree, dist, cavg, cmin, cmax];
    return array;
  }

  readData(json) {
		let array = json['data'];
		return array;
  }
}

var utils = new util();
module.exports = utils