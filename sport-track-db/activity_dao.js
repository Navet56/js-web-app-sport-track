class activity_dao {


	constructor() {
		if (activity_dao.exists) {
		return activity_dao.instance;
		}
		activity_dao.instance = this;
		activity_dao.exists = true;
		return this;
	}



  async insert(values){

      let db = require('./sport-track-db').db;

      let tmp = '(' + values.map((value) => '?').join(',') + ')';

       // prepare the SQL statement
      let query = "insert into activity(date, description, heureDeDebut, duree, distance, cardio_min, cardio_moyenne, cardio_max, my_account) VALUES " + tmp;

      let ret;

      let callback = function(){
        ret = this.lastID;
      }

      db.run(query, values, callback);

      

      return new Promise(resolve => {
        setTimeout(() => {
          resolve(ret);
        }, 200);
      });
	};

    update(key, values, callback){

    };

    delete(key, callback){

    };

    findAll(email, callback){
        let db = require('./sport-track-db').db;
        db.all("select * from Activity where my_account = '" + email + "'", callback);
    };

    findByKey(key, callback){};

}

var act_dao = new activity_dao();
module.exports = act_dao;
