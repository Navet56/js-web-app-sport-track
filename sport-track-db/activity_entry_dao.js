class activity_entry_dao {


	constructor() {
		if (activity_entry_dao.exists) {
		return activity_entry_dao.instance;
		}
		activity_entry_dao.instance = this;
		activity_entry_dao.exists = true;
		return this;
	}



  insert(values, callback){

      let db = require('./sport-track-db').db;

      let tmp = '(' + values.map((value) => '?').join(',') + ')';

       // prepare the SQL statement
      let query = "insert into data(time, cardio_frequency, longitude, latitude, altitude, my_activity) VALUES " + tmp;

      db.run(query, values, callback);

      return new Promise(resolve => {
        setTimeout(() => {
          resolve('resolved');
        }, 200);
      });


	}

    update(key, values, callback){

    };

    delete(key, callback){

    };

    findAll(callback){
      db.all("select * from Data", callback);
    };

    findByKey(key, callback){};

    

}

var act_entry_dao = new activity_entry_dao();
module.exports = act_entry_dao;
